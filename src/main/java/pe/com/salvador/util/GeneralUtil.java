package pe.com.salvador.util;

import org.springframework.ui.Model;

public class GeneralUtil {

    public static void setGenerix(Model model){
        model.addAttribute("nomEmpresa", "Automotriz El Salvador");
    }

    public static <T> T getFirstElementOf(Iterable<T> iterable) {
        return iterable.iterator().next();
    }
}
