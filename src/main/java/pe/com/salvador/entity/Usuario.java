package pe.com.salvador.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "usuarios", schema = "sysempresarial")
public class Usuario {

    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Integer idUsuario;
    private @Column String desNombre;
    private @Column String desApellido;
    private @Column String desUsuario;
    private @Column String desPassword;
    private @Column Integer idEmpresa;
    private @Column Integer indEstado;

    @ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinTable(
            name = "usuarios_roles",
            joinColumns = @JoinColumn(name = "id_usuario",referencedColumnName = "idUsuario"),
            inverseJoinColumns = @JoinColumn(name = "id_rol",referencedColumnName = "idRol")
    )
    private Collection<Rol> roles;
}
