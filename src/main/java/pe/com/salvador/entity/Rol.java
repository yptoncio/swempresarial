package pe.com.salvador.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "rol", schema = "sysempresarial")
public class Rol {

    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Integer idRol;
    private @Column String des_rol;
}
