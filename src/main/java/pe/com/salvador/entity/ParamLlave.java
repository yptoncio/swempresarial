package pe.com.salvador.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "paramllave", schema = "sysempresarial",
        uniqueConstraints = {@UniqueConstraint(columnNames={"idLlave"})})
public class ParamLlave {

    private @Id Integer idLlave;
    private @Column String desLlave;
}
