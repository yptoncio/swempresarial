package pe.com.salvador.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "inventario", schema = "sysempresarial")
public class Inventario {

    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Integer idProd;
    private @Column String desProd;
    private @Column Integer numStock;
    private @Column String desCodigo;
    private @Column String desUbicacion;
    private @Column BigDecimal decPrecio;
    private @Column Integer idCategoria;
    private @Column Date fecModif;

}
