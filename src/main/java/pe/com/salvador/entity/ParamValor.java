package pe.com.salvador.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "paramvalor", schema = "sysempresarial",
        uniqueConstraints = {@UniqueConstraint(columnNames={"idValor"})})
public class ParamValor {

    private @Id Integer idValor;
    private @Column String desValor;
    private @Column Integer idLlave;
    private @Column boolean indEstado;

}
