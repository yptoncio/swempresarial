package pe.com.salvador.controller;

import lombok.extern.log4j.Log4j2;
import org.hibernate.exception.JDBCConnectionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pe.com.salvador.entity.Inventario;
import pe.com.salvador.service.InventarioService;
import pe.com.salvador.util.GeneralUtil;

@Controller
@Log4j2
public class InventarioController {

    private static final String URL_BASE_INVENTARIO = "/inventario";

    @Autowired
    private InventarioService inventarioService;

    @GetMapping(URL_BASE_INVENTARIO)
    public String inventario(Model model){
        log.info("inventario");
        GeneralUtil.setGenerix(model);
        try {
            Iterable<Inventario> listInv = inventarioService.listar();
            log.info("listInv: " + listInv.toString());
            model.addAttribute("listInv", listInv);
        } catch (JDBCConnectionException es) {
            log.error("JDBO-"+es.getMessage());
            return "500";
        } catch (Exception e) {
            log.error(e.getMessage());
            return "500";
        }
        return "inventory";
    }

    @PostMapping(URL_BASE_INVENTARIO + "/grabar")
    public String grabarInventario(@ModelAttribute Inventario inventario, Model model){
        log.info("grabarInventario");
        GeneralUtil.setGenerix(model);
        log.info(inventario.toString());
        inventarioService.grabar(inventario);
        return "redirect:/inventario";
    }

    @GetMapping(URL_BASE_INVENTARIO + "/registro")
    public String mostrarFormularioDeRegistro(Model model) {
        log.info("mostrarFormularioDeRegistro");
        GeneralUtil.setGenerix(model);
        model.addAttribute("isNew", true);
        model.addAttribute("inventario", new Inventario());
        return "invRegistry";
    }


    @GetMapping(URL_BASE_INVENTARIO + "/editar/{idProd}")
    public String mostrarFormularioDeEdicion(@PathVariable String idProd, Model model) {
        log.info("mostrarFormularioDeEdicion");
        GeneralUtil.setGenerix(model);
        model.addAttribute("isNew", false);
        Inventario inventario = inventarioService.obtener(Integer.parseInt(idProd));
        model.addAttribute("inventario", inventario);
        return "invRegistry";
    }

}
