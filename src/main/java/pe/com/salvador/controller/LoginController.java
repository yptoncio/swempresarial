package pe.com.salvador.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import pe.com.salvador.util.GeneralUtil;

@Controller
public class LoginController {

    @GetMapping("/")
    public String cargarLogin(){
        return "login";
    }

    @GetMapping("/login")
    public String login(Model model){
        return "login";
    }

    @GetMapping("/index")
    public String index(Model model){
        GeneralUtil.setGenerix(model);
        return "inicio";
    }

}
