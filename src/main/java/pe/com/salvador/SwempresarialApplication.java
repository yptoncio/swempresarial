package pe.com.salvador;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwempresarialApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwempresarialApplication.class, args);
    }

}
