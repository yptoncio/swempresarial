package pe.com.salvador.service;

import pe.com.salvador.entity.Inventario;

public interface InventarioService {
    Inventario obtener(Integer idProd);
    Iterable<Inventario> listar();
    void grabar(Inventario inventario);
}
