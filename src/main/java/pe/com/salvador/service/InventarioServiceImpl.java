package pe.com.salvador.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.salvador.entity.Inventario;
import pe.com.salvador.repository.InventarioRepository;

@Service
public class InventarioServiceImpl implements InventarioService {

    @Autowired
    private InventarioRepository inventarioRepository;

    @Override
    public Inventario obtener(Integer idProd) {
        return inventarioRepository.findById(idProd).get();
    }

    @Override
    public Iterable<Inventario> listar() {
        return inventarioRepository.findAll();
    }

    @Override
    public void grabar(Inventario inventario) {
        inventarioRepository.save(inventario);
    }
}
