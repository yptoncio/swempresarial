package pe.com.salvador.repository;

import org.springframework.data.repository.CrudRepository;
import pe.com.salvador.entity.ParamLlave;

public interface ParamLlaveRepository extends CrudRepository<ParamLlave, Integer> {
}
