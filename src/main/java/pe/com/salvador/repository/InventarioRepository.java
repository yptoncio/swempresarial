package pe.com.salvador.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pe.com.salvador.entity.Inventario;

@Repository
public interface InventarioRepository extends CrudRepository<Inventario, Integer> {

}
