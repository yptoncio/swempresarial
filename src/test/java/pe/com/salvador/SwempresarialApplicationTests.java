package pe.com.salvador;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pe.com.salvador.entity.Inventario;
import pe.com.salvador.entity.ParamLlave;
import pe.com.salvador.entity.Usuario;
import pe.com.salvador.repository.InventarioRepository;
import pe.com.salvador.repository.ParamLlaveRepository;
import pe.com.salvador.repository.UsuarioRepository;

import java.util.List;

@SpringBootTest
class SwempresarialApplicationTests {

    @Autowired
    private InventarioRepository inventarioRepository;

    @Autowired
    private ParamLlaveRepository paramLlaveRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Test
    void contextLoads() {

//        Iterable<Inventario> listInv =  inventarioRepository.findAll();
//        System.out.println(listInv.toString());

        Iterable<ParamLlave> listParam = paramLlaveRepository.findAll();
        System.out.println("lista: " +  listParam.toString());

        Usuario user = usuarioRepository.findUsuarioByDesUsuario("admin");
        System.out.println("usuario: " + user);

    }

}
